/*
    Author: Anh Le
    Course: ICT-4510
    Date: 05/17/2020
    Description: This js file uses for displaying map, and showing pinned location. 
*/

// navigator.geolocation.getCurrentPosition(console.log, console.log)
const MAP_BOX_ACCESS_TOKEN = 'pk.eyJ1IjoiYW5odHVhbjcyNSIsImEiOiJja2FiOTJ0a3MwdWdyMzZtZ28zYTltdGZ6In0.QimIV-MiUq2yypoAIxmB_g'
// Univesity of Denver
const PROVIDED_LOCATION = {
  lat: 39.678121,
  long: -104.961753
}

// set Map to location and zoom level: 15
const mymap = L.map('mapid').setView([PROVIDED_LOCATION.lat, PROVIDED_LOCATION.long], 15)
// add tileLayer
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 20,
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1,
  accessToken: MAP_BOX_ACCESS_TOKEN
}).addTo(mymap)

// add market
const marker = L.marker([PROVIDED_LOCATION.lat, PROVIDED_LOCATION.long]).addTo(mymap)
marker.bindPopup('<b>University of Denver').openPopup()