/*
    Author: Anh Le
    Course: ICT-4510
    Date: 06/05/2020
    Description: this js file get information from fields to login'
*/

'use strict'

$(function () {
  checkSession()
  $('#form-login input').on('input', (e) => {
    $('#form-login .invalid-feedback').empty()
  })
  $('#form-login').submit(async (event) => {
    // onSubmit
    event.preventDefault()
    $('#form-login .spinner-border').show()
    Auth.login()
      .then(goToDashboard)
      .catch((err) => {
        let msg = 'The username or password supplied is not valid'
        if (typeof err === 'object') {
          msg = err.message
        }
        $('#form-login .invalid-feedback').html(msg)
      })
      .finally(() => {
        $('#form-login .spinner-border').hide()
      })
  })
})

const checkSession = () => {
  // if login => go to dashboard
  return Auth.hasLoggedIn() && goToDashboard()
}

const goToDashboard = () => {
  window.location = 'menu.html'
}