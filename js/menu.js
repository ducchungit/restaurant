'use strict'

$(function () {
  if (Auth.hasLoggedIn()) {
    $('.dashboard').show();
  } else {
    $('.dashboard').hide();
  }
  
  getMenuItems()
})

const getMenuItems = () => {
  const url = `${MENU_API}?api_key=${API_KEY}`
  $('#loading').show()
  fetch(url)
    .then((response) => {
      if (response.status === 200) {
        return response.json()
      }
    })
    .then((json) => {
      renderMenuItems(json.menu)
    })
    .catch((err) => {
      alert('Something went wrong, please try again')
      console.error(err)
    })
    .finally(() => {
      $('#loading').hide()
    })
}

const renderMenuItems = (menu) => {
  if (menu.length === 0) {
    $('#message').html('<div class="alert alert-info">No menu items found</div>')
    return false
  }
  $('#menu-items-list tbody').empty()
  menu.forEach((item) => {
    const html = `<tr>
        <td>${item.item}</td>
        <td>${item.description}</td>
        <td>${item.price}</td>
        </tr>`
    $('#menu-items-list tbody').append(html)
  })
  $('#menu-items-list').show()
}

const saveMenuItems = async () => {
  const user = Auth.getUser()
  const url = `${MENU_API}?api_key=${API_KEY}`

  const item = {
    item: $('#item').val(),
    description: $('#description').val(),
    price: $('#price').val()
  }

  let request = new Request(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'x-access-token': user.token
    },
    body: JSON.stringify(item),
    mode: 'cors'
  })

  const response = await fetch(request)
  if (response.status === 201) {
    // clear form fields
    $('#item').val(null)
    $('#description').val(null)
    $('#price').val(null)

    $('#message').html('<div class="alert alert-success">Menu Item Saved</div>')

    getMenuItems()

    setTimeout(() => {
      $('#message').html(null)
    }, 3000)
  } else if (response.status === 400) {
    const res = await response.json()
    let errors = '<ul>'

    for (let i = 0; i < res.errors.length; i++) {
      errors += '<li>' + res.errors[i].message + '</li>'
    }

    errors += '</ul>'

    $('#message').html(`<div class="alert alert-danger">${errors}</div>`)
    setTimeout(() => {
      $('#message').html(null)
    }, 7000)
  } else {
    $('#message').html('<div class="alert alert-danger">Menu Item NOT Saved</div>')
    setTimeout(() => {
      $('#message').html(null)
    }, 3000)
  }
}
