/*
    Author: Anh Le
    Course: ICT-4510
    Date: 06/05/2020
    Description: this js file handles login/logout and check login/logout status for users
*/

'use strict'
const USER_KEY = 'USER_INFO'

const Auth = new (function () {
  this.login = async () => {
    // get credentials information
    const username = $('#username').val()
    const password = $('#password').val()
    const credentials = {
      username,
      password
    }
    // request login
    const response = await fetch(LOGIN_API, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    })
    if (response.status === 401) { // request fail
      const res = await response.json()
      throw { status: response.status, message: res.message }
    }
    // request fail
    if (response.status !== 200) {
      throw 'The username or password are not correct'
    }
    // request success
    const userResponse = await response.json()
    const user = userResponse.user
    this.setUser(user)
    return user
  }

  this.setUser = (user) => {
    // save into sessionStorage
    return sessionStorage.setItem(USER_KEY, JSON.stringify(user))
  }

  this.getUser = () => {
    // get information from session storage
    const user = JSON.parse(sessionStorage.getItem(USER_KEY))
    if (user) setConfig(user)
    return user
  }

  this.hasLoggedIn = () => {
    // check is login or note
    return !!this.getUser()
  }

  this.logout = (target = 'login.html') => {
    // logout
    sessionStorage.removeItem(USER_KEY)
    window.location = target
    return true
  }
})();

(() => {
  // check login at start point
  if (Auth.hasLoggedIn()) {
    $('#login').hide()
    $('#navbar-right').show()
    const { first_name: firstName, last_name: lastName } = Auth.getUser()
    $('#full_name b').html(`${firstName} ${lastName}`)
  } else {
    $('login').show()
  }
})()