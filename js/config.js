/*
    Author: Anh Le
    Course: ICT-4510
    Date: 06/05/2020
    Description: this js file contains baseURL for request.
*/

'use strict'

// constant 
let API_KEY = 'ef94a0dee16ed8aa54fde114fc25ecde';
const BASE_API = 'https://ict4510.herokuapp.com/api/'
const MENU_API = BASE_API + 'menus'
const LOGIN_API = BASE_API + 'login'

const setConfig = (user) => {
  // save information for apikey
  API_KEY = user.api_key
  return true
}